package lecture1;

import java.util.List;

/**
 * This implementation tests all pairs in a list to see if any pair is a duplicate
 * @author Martin Vatshelle
 *
 */
public class SimpleDuplicateFinder implements DuplicateFinder<Object>{

	public <T extends Object> T findDuplicate(List<T> list) { //O(n^2) med LinkedList O(n^3)

		for(int i=0; i<list.size(); i++) { //n iterasjoner
			for(int j=0; j<i; j++) {//1+2+3+4...+n =O(n^2) iterasjoner
				if(list.get(i).equals(list.get(j))) { //O(1) med LinkedList O(i+j)
					return list.get(i); //O(1)
				}
			}
		}
		return null;
	}

//	public static void main(String[] args) {
//		List<String> brukernavn = StringListGenerator.generateStringList(1000);
//		System.out.println(brukernavn);
//
//		long start = System.currentTimeMillis();
//		String duplicate = findDuplicate(brukernavn);
//		long stop = System.currentTimeMillis();
//		long time1 = (stop-start);
//		System.out.println(duplicate);
//
//		brukernavn.add(3, "Martin");
//		brukernavn.add(7,"Martin");
//		System.out.println(brukernavn);
//
//		start = System.currentTimeMillis();
//		duplicate = findDuplicate(brukernavn);
//		stop = System.currentTimeMillis();
//		long time2 = (stop-start);
//		
//		System.out.println(duplicate);
//		System.out.println("Time: "+time1+"ms and "+time2+"ms");
//	}

}
