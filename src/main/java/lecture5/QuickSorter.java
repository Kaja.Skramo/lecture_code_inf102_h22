package lecture5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lecture1.StringListGenerator;

public class QuickSorter implements Sorter {

	public static void main(String[] args) {
		QuickSorter qs = new QuickSorter();
		List<String> inputStrings = StringListGenerator.generateStringList(100000);
		CodeTimer.timeMethod(qs::sort, inputStrings);
		CodeTimer.timeMethod(qs::sort, inputStrings);
	}
	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) { //O(n^2)
		Collections.shuffle(list);
		if(list.size()<=1)
			return;
		T pivot = list.get(0);
		List<T> smaller = new ArrayList<T>();
		List<T> larger = new ArrayList<T>();
		List<T> equal = new ArrayList<T>();
		for(T t : list) { //n iterations //O(n)
			if(t.compareTo(pivot)<0) {
				smaller.add(t); //O(1)
			}
			else {
				if(t.compareTo(pivot)>0) {
					larger.add(t);
				}
				else {
					equal.add(t);
				}
			}
		}
		this.sort(smaller);
		this.sort(larger);

		int index = 0;
		for(T t : smaller) {
			list.set(index++, t);
		}
		for(T t : equal) {
			list.set(index++, t);
		}
		for(T t : larger) {
			list.set(index++, t);
		}
	}

}
