package lecture5;

import java.util.Collections;
import java.util.List;

public class JavaSort implements Sorter {

	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) {
		Collections.sort(list);
	}

}
