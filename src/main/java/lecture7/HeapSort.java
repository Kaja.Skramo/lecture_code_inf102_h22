package lecture7;

import java.util.List;
import java.util.PriorityQueue;

import lecture5.Sorter;

public class HeapSort implements Sorter {

	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) {
		PriorityQueue<T> pq = new PriorityQueue<>();
		for(T t : list) {
			pq.add(t);
		}
		int i=0;
		while(!pq.isEmpty()) {
			T min = pq.remove();
			list.set(i, min);
			i++;
		}

	}

}
