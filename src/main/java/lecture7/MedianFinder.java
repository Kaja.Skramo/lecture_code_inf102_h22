package lecture7;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MedianFinder<T extends Comparable<? super T>> implements IMedianFinder<T> {

	PriorityQueue<T> low = new PriorityQueue<>(Comparator.reverseOrder());
	PriorityQueue<T> hi = new PriorityQueue<>();
	
	@Override
	public T getMedian() {
		if(low.size()>hi.size())
			return low.peek();
		if(hi.size()>low.size())
			return hi.peek();
		
		return low.peek();
	}

	@Override
	public void add(T element) {

		if(low.isEmpty() || element.compareTo(low.peek())<=0) {
			low.add(element);
		}
		else
			hi.add(element);
		
		balance();
	}

	private void balance() {
		while(hi.size()>low.size()) {
			low.add(hi.remove());
		}
		while(low.size()>hi.size()) {
			hi.add(low.remove());
		}
	}
	
	public static void main(String[] args) {
		MedianFinder<Integer> mf = new MedianFinder<>();
		mf.add(7);
		mf.add(3);
		mf.add(9);
		mf.add(12);
		mf.add(4);
		
		System.out.println(mf.getMedian());
		
	}

}
