package lecture4;

import java.util.Comparator;

public class TverrsumComparator implements Comparator<Integer>{

	public static int tverrsum(int num) {
		int sum = 0;
		while(num>0) {
			sum += num%10; //modulo 10
			num /=10;
		}
		return sum;
	}

	@Override
	public int compare(Integer a, Integer b) {
		return Integer.compare(tverrsum(a), tverrsum(b));
	}
}
