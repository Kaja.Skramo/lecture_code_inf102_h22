package lecture2;

import java.util.LinkedList;
import java.util.List;

public class LinkedListReverser implements ListReverser {

	@Override
	public <T> List<T> reverse(List<T> list) {
		List<T> newList = new LinkedList<>();
		for(T t : list) {
			newList.add(0,t);
		}
		return newList;
	}

}
