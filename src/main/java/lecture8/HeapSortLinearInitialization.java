package lecture8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;

import lecture5.Sorter;

public class HeapSortLinearInitialization implements Sorter {

	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) {
		Comparator<? super T> comp = Comparator.naturalOrder();
		List<Box<T>> boxlist = new ArrayList<>();
		for(T t : list)
			boxlist.add(new Box<T>(t,comp));
		PriorityQueue<Box<T>> pq = new PriorityQueue<>(boxlist);
		
		
		int i=0;
		while(!pq.isEmpty()) {
			T min = pq.remove().value;
			list.set(i, min);
			i++;
		}

	}

}

class Box<T extends Comparable<? super T>> implements Comparable<Box<T>>{

	Comparator<? super T> comp;
	T value;
	
	public Box(T value, Comparator<? super T> comp2) {
		this.comp = comp2;
		this.value = value;
	}
	
	@Override
	public int compareTo(Box<T> box) {
		return comp.compare(this.value, box.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(comp, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Box other = (Box) obj;
		return Objects.equals(comp, other.comp) && Objects.equals(value, other.value);
	}
	
	
	
}
