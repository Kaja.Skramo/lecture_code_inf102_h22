package lecture6;

import java.util.ArrayList;
import java.util.List;

import lecture5.Sorter;

public class MergeSort implements Sorter {

	@Override
	public <T extends Comparable<? super T>> void sort(List<T> list) {
		if(list.size()<=1)
			return;
		// Divide
		List<T> first = new ArrayList<T>();
		List<T> second = new ArrayList<T>();
		for(int i=0; i<list.size(); i++) { //n iterations
			if(i%2==0)
				first.add(list.get(i));//O(1)
			else
				second.add(list.get(i));//O(1)
		}
		
		//recursion
		sort(first);
		sort(second);
		
		//Merge
		for(int i=list.size()-1; i>=0; i--) {//n iterations
			//check if next smallest element is in first
			if(!first.isEmpty() && (second.isEmpty() || first.get(first.size()-1).compareTo(second.get(second.size()-1))<0)) {//O(1)
				list.set(i, first.remove(first.size()-1));//O(n)
			}
			else {
				list.set(i, second.remove(second.size()-1));
			}
		}

	}

}
